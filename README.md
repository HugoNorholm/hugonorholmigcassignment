HugoNorholmIGCassignment

Description:
Assignment to implement and deploy an application to parse and view igc files.
deployed to: https://hugonorholmigcinfo.herokuapp.com

To deploy the application ourself you would need to install go.
Place this directory within your GOPATH
Set PORT to what you want it to be 
Then Run "go run main.go" in your terminal.