package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/marni/goigc"
	"github.com/rickb777/date/period"
)

// APIMeta Json struct for API
type APIMeta struct {
	Uptime  period.Period `json:"uptime,omitempty"`
	Info    string        `json:"info,omitempty"`
	Version string        `json:"version,omitempty"`
}

// Track Json struct for tracks
type Track struct {
	ID          string  `json:"id,omitempty"`
	Hdate       string  `json:"H_date,omitempty"`
	Pilot       string  `json:"pilot,omitempty"`
	Glider      string  `json:"glider,omitempty"`
	GliderID    string  `json:"gliderid,omitempty"`
	TrackLenght float64 `json:"tracklength"`
}

// URLBody Json struct for URLs
type URLBody struct {
	URL string `json:"URL,omitempty"`
}

// IDResponse Json struct for ID
type IDResponse struct {
	ID string `json:"id,omitempty"`
}

var tracks []Track
var startTime time.Time

//GetAPI Send API info as JSON
func GetAPI(w http.ResponseWriter, r *http.Request) {
	uptime := period.Between(time.Now(), startTime)
	x := APIMeta{Uptime: uptime, Info: "Service for IGC tracks", Version: "V1"}
	json.NewEncoder(w).Encode(x)
}

//PostIgc Takes URL in json and gets igc file parses it and stores it in array
func PostIgc(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var u URLBody
	err := decoder.Decode(&u)
	if err != nil {
		panic(err)
	}

	s := u.URL
	treck, err := igc.ParseLocation(s)

	tracks = append(tracks, Track{ID: treck.UniqueID, Hdate: treck.Header.Date.String(), Pilot: treck.Pilot, Glider: treck.GliderType, GliderID: treck.GliderID, TrackLenght: treck.Task.Distance()})

	i := IDResponse{ID: treck.UniqueID}
	json.NewEncoder(w).Encode(i)
}

//GetIgc Sends all tracks
func GetIgc(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(tracks)
}

//GetIgcID Sends all info of a track with a given id
func GetIgcID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, item := range tracks {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("400 - Bad Request"))
}

//GetIgcIDField Sends specific info of a track with a given id
func GetIgcIDField(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, item := range tracks {
		if item.ID == params["id"] {
			switch params["field"] {
			case "pilot":
				json.NewEncoder(w).Encode(item.Pilot)
				return
			case "glider":
				json.NewEncoder(w).Encode(item.Glider)
				return
			case "glider_id":
				json.NewEncoder(w).Encode(item.GliderID)
				return
			case "calculated_total_track_length":
				json.NewEncoder(w).Encode(item.TrackLenght)
				return
			case "H_date":
				json.NewEncoder(w).Encode(item.Hdate)
				return
			}
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - NOT FOUND"))
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("400 - Bad Request"))
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return "", fmt.Errorf("$PORT not set")
	}
	return ":" + port, nil
}

func main() {
	startTime = time.Now()
	addr, err := determineListenAddress()
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()

	r.HandleFunc("/igcinfo/api", GetAPI).Methods("GET")
	r.HandleFunc("/igcinfo/api/igc", PostIgc).Methods("POST")
	r.HandleFunc("/igcinfo/api/igc", GetIgc).Methods("GET")
	r.HandleFunc("/igcinfo/api/igc/{id}", GetIgcID).Methods("GET")
	r.HandleFunc("/igcinfo/api/igc/{id}/{field}", GetIgcIDField).Methods("GET")

	log.Printf("Listening on %s...\n", addr)
	if err := http.ListenAndServe(addr, r); err != nil {
		panic(err)
	}
}
